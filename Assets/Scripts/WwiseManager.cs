﻿using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class WwiseManager
{
    public static uint PostDynamicEventReference(WwiseDynamicEventReference reference, GameObject gameObject)
    {
        return PostDynamicEvent(reference.EventID, gameObject, reference.TargetStates);
    }
    
    public static uint PostDynamicEvent(uint eventID, GameObject gameObject, uint[] targetStates)
    {
        var sequenceID = AkSoundEngine.DynamicSequenceOpen(gameObject);

        AkPlaylist playlist = AkSoundEngine.DynamicSequenceLockPlaylist(sequenceID);

        var nodeID = AkSoundEngine.ResolveDialogueEvent(eventID, targetStates, (uint)targetStates.Length);
        playlist.Enqueue(nodeID);

        AkSoundEngine.DynamicSequenceUnlockPlaylist(sequenceID);

        AkSoundEngine.DynamicSequencePlay(sequenceID);

        AkSoundEngine.DynamicSequenceClose(sequenceID);

        return sequenceID;
    }

        
    public static uint PostDynamicEvent(string dynamicEventName, GameObject gameObject, uint[] targetStates)
    {
        return PostDynamicEvent(AkSoundEngine.GetIDFromString(dynamicEventName), gameObject, targetStates);
    }

    public static AKRESULT PauseDynamicEvent(uint sequenceID)
    {
        return AkSoundEngine.DynamicSequencePause(sequenceID);
    }
        
    public static AKRESULT ResumeDynamicEvent(uint sequenceID)
    {
        return AkSoundEngine.DynamicSequenceResume(sequenceID);
    }
        
    public static AKRESULT StopDynamicEvent(uint sequenceID)
    {
        return AkSoundEngine.DynamicSequenceStop(sequenceID);
    }
        
}