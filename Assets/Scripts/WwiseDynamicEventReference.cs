﻿using System;
using UnityEngine;

namespace DefaultNamespace
{
    [CreateAssetMenu(fileName = "WwiseDynamicEventReference", menuName = "Wwise/DynamicEventReference")]
    public class WwiseDynamicEventReference : ScriptableObject
    {
        [SerializeField] private string dynamicEventName; 
        [SerializeField] private AK.Wwise.State[] targetStates;

        public uint EventID { get; private set; }
        public uint[] TargetStates { get; private set; }
        private void OnEnable()
        {
            EventID = AkSoundEngine.GetIDFromString(dynamicEventName);
            TargetStates = new uint[targetStates.Length];
            for (int i = 0; i < targetStates.Length; i++)
            {
                TargetStates[i] = targetStates[i].Id;
            }
        }
    }
}