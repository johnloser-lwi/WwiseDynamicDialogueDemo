using UnityEngine;

public class SimpleCharacterController : MonoBehaviour
{
    [SerializeField] private float speed = 100.0f;
    [SerializeField] private float runMultiplier = 1.5f;
    [SerializeField] private float angularSpeed = 100.0f;
    [SerializeField][Range(0.0f, 1.0f)] private float moveThreshold = 0.2f;
    [SerializeField][Range(0.0f, 0.5f)] private float deadZone = 0.1f;
    
    private Vector3 transformVector = Vector3.zero;
    private Quaternion targetRotation = Quaternion.identity;
    
    private bool isRunning = false;

    // Update is called once per frame
    void Update()
    {
        InputHandler();
    }

    private void FixedUpdate()
    {
        DoTransform(Time.fixedDeltaTime);
    }

    private void InputHandler()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            isRunning = true;
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            isRunning = false;
        }
        
        var z = Input.GetAxis("Vertical");
        var x = Input.GetAxis("Horizontal");

        if (Mathf.Abs(z) < deadZone) z = 0.0f;
        if (Mathf.Abs(x) < deadZone) x = 0.0f;
        
        transformVector.x = x;
        transformVector.z = z;
    }

    private void DoTransform(float deltaTime)
    {
        var speedScale = transformVector.magnitude;
        if (speedScale < moveThreshold) return;
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, deltaTime * angularSpeed);
        var direction = transformVector.normalized;
        targetRotation = Quaternion.LookRotation(direction);
        transform.Translate(Vector3.forward * speed * (isRunning ? runMultiplier : 1.0f) * speedScale * deltaTime);
    }
}
