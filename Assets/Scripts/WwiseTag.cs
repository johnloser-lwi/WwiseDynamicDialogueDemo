﻿using System;
using UnityEngine;

[Flags]
public enum WwiseTagCategory
{
    None = 0,
    Ground = 1,
    Wall = 2,
    Character = 4,
    Weapon = 8,
}

public class WwiseTag : MonoBehaviour
{
    [SerializeField] private WwiseTagCategory category;
    [SerializeField] private AK.Wwise.State targetState;
    
    public WwiseTagCategory Category => category;
    public uint TargetState => targetState is null ? 0 : targetState.Id;
}