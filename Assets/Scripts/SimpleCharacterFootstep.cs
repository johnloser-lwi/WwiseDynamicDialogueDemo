﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

public class SimpleCharacterFootstep : MonoBehaviour
{
    [SerializeField] private string footstepEvent;
    [SerializeField] private float walkingTriggerRate = 0.5f;
    [SerializeField] private float runningTriggerRate = 0.25f;

    [SerializeField][Range(0.0f, 100.0f)] private float volume = 100.0f;
    
    [SerializeField] private AK.Wwise.State defaultFootstep;
    [SerializeField] private AK.Wwise.State runMotion;
    [SerializeField] private AK.Wwise.State walkMotion;
       
    private uint footstepEventID;
    private float triggerTimer = 0.0f;

    private uint runMotionID;
    private uint walkMotionID;

    private float triggerRate;

    uint[] stateArray = new uint[2]; // [Motion, Footstep]

    private Rigidbody rig;

    private uint triggerCount = 0;
    
    private void Start()
    {
        rig = GetComponent<Rigidbody>();
        
        footstepEventID = AkSoundEngine.GetIDFromString(footstepEvent);
        runMotionID = runMotion.Id;
        walkMotionID = walkMotion.Id;

        stateArray[0] = walkMotionID;
        stateArray[1] = defaultFootstep.Id;
        
        triggerRate = walkingTriggerRate;
        
        AkSoundEngine.SetRTPCValue("RTPC_Volume", volume);
    }

    public void Update()
    {
        TriggerFootstep(Time.deltaTime);
        UpdateStates();
    }

    private void UpdateStates()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            stateArray[0] = runMotionID;
            triggerRate = runningTriggerRate;
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            stateArray[0] = walkMotionID;
            triggerRate = walkingTriggerRate;
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.TryGetComponent(out WwiseTag tag)) return;
        // if not has flag return
        if ((tag.Category & WwiseTagCategory.Ground) == 0) return;
        if (tag.TargetState == 0)
        {
            stateArray[1] = defaultFootstep.Id;
            return;
        }
        stateArray[1] = tag.TargetState;
        triggerCount++;
    }

    private void OnTriggerExit(Collider other)
    {
        triggerCount--;
        if (triggerCount != 0) return;
        
        stateArray[1] = defaultFootstep.Id;
    }

    private void TriggerFootstep(float deltaTime)
    {
        triggerTimer += deltaTime;
        if (triggerTimer < triggerRate || rig.velocity.sqrMagnitude == 0) return;
        
        triggerTimer = 0.0f;
        WwiseManager.PostDynamicEvent(footstepEventID, gameObject, stateArray);
    }
}