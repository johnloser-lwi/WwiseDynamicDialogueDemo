using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class WeaponSwitch : MonoBehaviour
{
    [SerializeField] private string impactEvent = "Play_Impact";
    [SerializeField] private float range = 1.0f;
    
    [SerializeField] private GameObject woodWeapon;
    [SerializeField] private GameObject stoneWeapon;
    [SerializeField] private GameObject ironWeapon;
    
    [SerializeField] private WeaponMaterial currentWeaponMaterial;

    [SerializeField] private string attackAnimationState = "SwingWeapon";

    private Animator weaponAnimator;

    private GameObject character;

    private GameObject currentWeapon;

    public enum WeaponMaterial
    {
        Wood,
        Stone,
        Iron
    }
    
    // Start is called before the first frame update
    void Start()
    {
        SetWeapon(currentWeaponMaterial);
        weaponAnimator = GetComponent<Animator>();
        character = transform.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SetWeapon(WeaponMaterial.Wood);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SetWeapon(WeaponMaterial.Stone);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            SetWeapon(WeaponMaterial.Iron);
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            weaponAnimator.Play(attackAnimationState);
            DetectHit();
        }
    }

    void DetectHit()
    {
        Physics.Raycast(character.transform.position, character.transform.forward, out RaycastHit hit, range);

        if (hit.collider == null) return;
        
        if (!hit.collider.gameObject.TryGetComponent(out WwiseTag tag)) return;
        
        if (!tag.Category.HasFlag(WwiseTagCategory.Character)) return;
        
        Debug.Log("Hit " + hit.collider.gameObject.name);
        if (!currentWeapon.TryGetComponent(out WwiseTag weaponTag)) return;
        var weaponMaterial = weaponTag.TargetState;
        WwiseManager.PostDynamicEvent(impactEvent, hit.collider.gameObject,
            new[] {weaponMaterial, tag.TargetState});
    }

    void SetWeapon(WeaponMaterial weaponMaterial)
    {
        bool woodWeaponVisible = false;
        bool stoneWeaponVisible = false;
        bool ironWeaponVisible = false;
        
        switch (weaponMaterial)
        {
            case WeaponMaterial.Wood:
                woodWeaponVisible = true;
                currentWeaponMaterial = WeaponMaterial.Wood;
                currentWeapon = woodWeapon;
                break;
            case WeaponMaterial.Stone:
                stoneWeaponVisible = true;
                currentWeaponMaterial = WeaponMaterial.Stone;
                currentWeapon = stoneWeapon;
                break;
            case WeaponMaterial.Iron:
                ironWeaponVisible = true;
                currentWeaponMaterial = WeaponMaterial.Iron;
                currentWeapon = ironWeapon;
                break;
        }
        
        SetVisibility(woodWeapon, woodWeaponVisible);
        SetVisibility(stoneWeapon, stoneWeaponVisible);
        SetVisibility(ironWeapon, ironWeaponVisible);
    }

    void SetVisibility(GameObject weapon, bool visible)
    {
        weapon.SetActive(visible);
    }
}
