# WwiseDynamicDialogueDemo
A concept project that uses only dynamic dialogue event.
<br/>
[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/99pnF7K8QUM/0.jpg)](https://www.youtube.com/watch?v=99pnF7K8QUM)

## Requirements
Unity 2022.1.18f1
<br/>
Wwise 2022.1.6

## Controls
Use W,S,A,D to move around. Hold left shift to sprint.
<br/>
Click left mouse button to attack.
<br/>
Use 1,2,3 to switch weapon.
